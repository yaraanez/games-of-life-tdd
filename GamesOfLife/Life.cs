﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GamesOfLife
{
    public class Life
    {
        public bool?[,] Tablero { get; set; }     

        public int Width { get; }
        public int Height { get; }

        public Life(int width, int height)
        {
            this.Width = width;
            this.Height = height;            
            this.Tablero = new bool?[width, height];
        }

        public void Init(List<KeyValuePair<int, int>> initialState)
        {
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    this.Tablero[i, j] = initialState.Any(x => x.Key == i && x.Value == j);
                }
            }
        }

        public void Jugar()
        {
            bool?[,] TempTablero = new bool?[Width, Height];

            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    int CelulasVivas = 0;
                    CelulasVivas += EstadoCelula(i - 1, j - 1);
                    CelulasVivas += EstadoCelula(i, j - 1);
                    CelulasVivas += EstadoCelula(i + 1, j - 1);
                    CelulasVivas += EstadoCelula(i + 1, j);
                    CelulasVivas += EstadoCelula(i + 1, j + 1);
                    CelulasVivas += EstadoCelula(i, j + 1);
                    CelulasVivas += EstadoCelula(i - 1, j + 1);
                    CelulasVivas += EstadoCelula(i - 1, j);

                    if (Tablero[i, j] ?? false)
                    {
                        TempTablero[i, j] = CelulasVivas == 2 || CelulasVivas == 3;
                    }
                    else
                    {
                        TempTablero[i, j] = CelulasVivas == 3;
                    }
                }
            }
            this.Tablero= TempTablero;
        }

        private int EstadoCelula(int i, int j)
        {
            return CelulaViVa(i, j) ? 1 : 0;
        }

        private bool CelulaViVa(int i, int j)
        {
            if (i >= 0 && j >= 0 && i < Width && j < Height)
            {
                return this.Tablero[i, j] ?? false;
            }
            return false;
        }
    }
}
