Ejecutar las pruebas del proyecto "TDD" -> "UnitTest1".

Se realiz� la implementaci�n de con forme las reglas del juego:
    * Si una c�lula esta viva, seguir� viva si tiene 2 o 3 c�lulas vivas como vecinas.
    * Si una c�lula esta viva, morir� si tiene menos de 2 c�lulas vivas como vecinas.
    * Si una c�lula esta viva, morir� si tiene m�s de 3 c�lulas vivas como vecinas.
    * Si una c�lula esta muerta, renacer� solo si tiene 3 c�lulas vivas como vecinas.
    * Si una c�lula esta muerta, seguir� muerta si tiene menos de 3 c�lulas vivas como vecinas.
    * Si una c�lula esta muerta, seguir� muerta si tiene m�s de 3 c�lulas vivas como vecinas.
    
Se utiliz� una matriz de booleanos nullables, que permiten almacenar la estructura de las c�lulas.

Se implementaron 6 casos de pruebas, los cuales proveen la siguiente informaci�n del juego:
    * Dimenciones del tablero (new (width, height)).
    * Inicializaci�n de c�lulas vivas (Init).
    * Definir resultado esperado para una posici�n especifica (i : eje x, j eje y).
    * Realizar iteraci�n del juego (jugar).
    * Obtener estado resultante del tablero (CelulaViVa).
    * Comprar resultado esperado con resultado de c�lula.
    
