﻿using System;
using System.Collections.Generic;
using GamesOfLife;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TDD
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CelulaVivaY2VecinasVivas()
        {
            Life life = new Life(5, 5);         

            life.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(0, 1),
                new KeyValuePair<int, int>(1, 2),
                new KeyValuePair<int, int>(2, 1)
            });
            //True= Viva, Falsa= Muerta
            bool? expectativa = true;

            life.Jugar();

            bool? resultado = life.Tablero[1, 2];

            Assert.AreEqual(expectativa, resultado);
        }

        [TestMethod]
        public void CelulaVivaY1VecinaViva()
        {
            Life life = new Life(5, 5);

            life.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(0, 1),
                new KeyValuePair<int, int>(1, 2)
            });
            //True= Viva, False= Muerta
            bool? expectativa = false;

            life.Jugar();

            bool? resultado = life.Tablero[0, 1];

            Assert.AreEqual(expectativa, resultado);
        }

        [TestMethod]
        public void CelulaVivaY4VecinasVivas()
        {
            Life life = new Life(5, 5);

            life.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(1, 2),
                new KeyValuePair<int, int>(2, 1),
                new KeyValuePair<int, int>(3, 0),
                new KeyValuePair<int, int>(3, 1),
                new KeyValuePair<int, int>(3, 2)
            });
            //True= Viva, False= Muerta
            bool? expectativa = false;

            life.Jugar();

            bool? resultado = life.Tablero[2, 1];

            Assert.AreEqual(expectativa, resultado);
        }

        [TestMethod]
        public void CelulaMuertaY3VecinasVivas()
        {
            Life life = new Life(5, 5);

            life.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(0, 1),
                new KeyValuePair<int, int>(1, 2),
                new KeyValuePair<int, int>(2, 1)
            });
            //True= Viva, False= Muerta
            bool? expectativa = true;

            life.Jugar();

            bool? resultado = life.Tablero[1, 1];

            Assert.AreEqual(expectativa, resultado);
        }

        [TestMethod]
        public void CelulaVivaY3VecinasVivas()
        {
            Life life = new Life(5, 5);

            life.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(2, 1),
                new KeyValuePair<int, int>(3, 1),
                new KeyValuePair<int, int>(3, 2),
                new KeyValuePair<int, int>(3, 3)
            });
            //True= Viva, False= Muerta
            bool? expectativa = true;

            life.Jugar();

            bool? resultado = life.Tablero[3, 2];

            Assert.AreEqual(expectativa, resultado);
        }

        [TestMethod]
        public void CelulaMuertaY2VecinasVivas()
        {
            Life life = new Life(5, 5);

            life.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(0, 1),
                new KeyValuePair<int, int>(2, 1)
            });
            //True= Viva, False= Muerta
            bool? expectativa = false;

            life.Jugar();

            bool? resultado = life.Tablero[1, 0];

            Assert.AreEqual(expectativa, resultado);
        }
    }


}
